<?php

@include 'config.php';

$id = $_GET['edit'];

if(isset($_POST['update_game'])){

   $game_hall = $_POST['game_hall'];
   $game_date = $_POST['game_date'];

   $game_logo1 = $_FILES['game_logo1']['name'];
   $game_logo1_tmp_name = $_FILES['game_logo1']['tmp_name'];
   $game_logo1_folder = 'game_uploaded/'.$game_logo1;
    
   $game_team1 = $_POST['game_team1'];
   $game_score1 = $_POST['game_score1'];
   $game_score2 = $_POST['game_score2'];
   $game_team2 = $_POST['game_team2'];

   $game_logo2 = $_FILES['game_logo2']['name'];
   $game_logo2_tmp_name = $_FILES['game_logo2']['tmp_name'];
   $game_logo2_folder = 'game_uploaded/'.$game_logo2;

   if(empty($game_hall) || empty($game_date) || empty($game_team1) || empty($game_score1) || empty($game_score2) || empty($game_team2)){
      $message[] = 'Completați toate câmpurile';    
   }else{

      $update_data = "UPDATE matches SET hall='$game_hall', date='$game_date', team1='$game_team1', score1='$game_score1', score2='$game_score2', team2='$game_team2' WHERE id = '$id'";
      
      if(!empty($game_logo1) && !empty($game_logo2))
      {
         $update_data = "UPDATE matches SET logo1='$game_logo1', logo2='$game_logo2'  WHERE id = '$id'";
         
      }
      else
      {
         if(!empty($game_logo1) && empty($game_logo2))
         {
            $update_data = "UPDATE matches SET logo1='$game_logo1'  WHERE id = '$id'";
         
         }
         else
         {
            if(empty($game_logo1) && !empty($game_logo2))
            {
             $update_data = "UPDATE matches SET logo2='$game_logo2'  WHERE id = '$id'";
         
            }

         }
      }
      $upload = mysqli_query($conn, $update_data);
      if($upload)
      {
            move_uploaded_file($game_logo2_tmp_name, $game_logo2_folder);
            move_uploaded_file($game_logo1_tmp_name, $game_logo1_folder);
            header('location:crud-games.php');
      }else
      {
            $$message[] = 'Completați toate câmpurile'; 
      }
   }
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Modifică meciul </title>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCrud.css">
   <link rel="stylesheet" href="meniu.css">
   
</head>
<body>
<div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m2">
                    <a class="ancora" href="lot.php">LOT</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="meciuri.php">MECIURI</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>
</div>
<?php
   if(isset($message)){
      foreach($message as $message){
         echo '<span class="message">'.$message.'</span>';
      }
   }
?>

<div class="container">


<div class="admin-product-form-container centered">

   <?php
      
      $select = mysqli_query($conn, "SELECT * FROM matches WHERE id = '$id'");
      while($row = mysqli_fetch_assoc($select)){

   ?>
   
   <form action="" method="post" enctype="multipart/form-data">
      <h3 class="title">Editați informațiile despre meci</h3>
      <input type="text" class="box" name="game_hall" value="<?php echo $row['hall']; ?>" placeholder="Introduceți sala">
      <input type="date" class="box" name="game_date" value="<?php echo $row['date']; ?>" placeholder="Introduceți data">
      <img src="<?php echo "game_uploaded/".$row['logo1']; ?>" width="100px;" >
      <input type="file" class="box" name="game_logo1" accept="image/png, image/jpeg, image/jpg">
      <input type="text" class="box" name="game_team1" value="<?php echo $row['team1']; ?>" placeholder="Introduceți echipa A">
      <input type="text" class="box" name="game_score1" value="<?php echo $row['score1']; ?>" placeholder="Introduceți scorul echipei A">
      <input type="text" class="box" name="game_score2" value="<?php echo $row['score2']; ?>" placeholder="Introduceți scorul echipei B">
      <input type="text" class="box" name="game_team2" value="<?php echo $row['team2']; ?>" placeholder="Introduceți echipa B">
      <img src="<?php echo "game_uploaded/".$row['logo2']; ?>" width="100px;" >
      <input type="file" class="box" name="game_logo2" accept="image/png, image/jpeg, image/jpg, image/webp, image/html, image/svg">
      <input type="submit" value="UPDATEAZĂ MECIUL" name="update_game" class="btn">
      <a href="crud-games.php" class="btn">ÎNAPOI</a>
   </form>
   


   <?php }; ?>

   

</div>

</div>

</body>
</html>