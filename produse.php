<?php

@include 'config.php';

if(isset($_POST['add_to_cart'])){

   $product_name = $_POST['product_name'];
   $product_price = $_POST['product_price'];
   $product_image = $_POST['product_image'];
   $product_quantity = 1;

   $select_cart = mysqli_query($conn, "SELECT * FROM `cart` WHERE name = '$product_name'");

   if(mysqli_num_rows($select_cart) > 0){
      $message[] = 'Produsul a fost deja adăugat în coș!';
      
   }else{
      $insert_product = mysqli_query($conn, "INSERT INTO `cart`(name, price, image, quantity) VALUES('$product_name', '$product_price', '$product_image', '$product_quantity')");
      $message[] = 'Produsul a fost adăugat în coș!';
   }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Articole de vânzare</title>
   <link rel="stylesheet" href="styleProduse.css">
   <link rel ="stylesheet" href ="meniu.css"> 
   <link href="https://fonts.googleapis.com/css2?family=Young+Serif&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
   
</head>
<body>
<div class="menu">
        <div class="left-menu">
            <img class="logo-partizan-init" src="poze/logo-partizan.png">
            <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
        </div>
        <div class="mijloc-menu">
            <div class="m1">
                <a class="ancora" href="index.html">ACASĂ</a>
            </div>
            <div class="m2">
                <a class="ancora" href="lot.php">LOT</a>
            </div>
            <div class="m3">
                <a class="ancora" href="meciuri.php">MECIURI</a>
            </div>
            <div class="m4">
                <a class="ancora" href="adauga.html">ADAUGĂ</a>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a> 
            <div class="m4">
               <?php
                  $select_rows = mysqli_query($conn, "SELECT * FROM `cart`") or die('query failed');
                  $row_count = mysqli_num_rows($select_rows);
               ?>
            <a href="cos.php">
                <button class="cos" style="cursor: pointer;"><i class="fas fa-shopping-cart"></i> <?php echo $row_count; ?></button>
            </a>
            </div>
        </div>
        
    </div>

<?php

if(isset($message)){
   foreach($message as $message){
      echo '<div style="
        top:0; left:0;
        z-index: 10000;
        border-radius: 10px;
        background-color: white;
        font-family: sans-serif;
        font-size: 20px;
        font-weight: bold;
        color: black;
        padding:20px 15px;
        margin: 0px auto 0px auto;
        max-width: 1200px;
        display: flex;
        align-items: center;
        justify-content: space-between;"><span>'.$message.'</span> <i class="fas fa-times" onclick="this.parentElement.style.display = `none`;"></i> </div>';
   };
};

?>

   <div class="zona-de-produse">

      <?php
      
      $select_products = mysqli_query($conn, "SELECT * FROM `products`");
      if(mysqli_num_rows($select_products) > 0){
         while($fetch_product = mysqli_fetch_assoc($select_products)){
      ?>

      <form action="" method="post">
         <div class="box">
            <img src="product_uploaded/<?php echo $fetch_product['image']; ?>" class="imagine-produs">
            <h3 class="produs"><?php echo $fetch_product['name']; ?></h3>
            <div class="pret"><?php echo $fetch_product['price']; ?> lei</div>
            <input type="hidden" name="product_name" value="<?php echo $fetch_product['name']; ?>">
            <input type="hidden" name="product_price" value="<?php echo $fetch_product['price']; ?>">
            <input type="hidden" name="product_image" value="<?php echo $fetch_product['image']; ?>">
            <input type="submit" style="cursor: pointer;" class="buton" value="ADAUGĂ ÎN COȘ" name="add_to_cart">
         </div>
      </form>

      <?php
         };
      };
      ?>

   </div>

</body>
</html>