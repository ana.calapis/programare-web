<?php

@include 'config.php';

$id = $_GET['edit'];

if(isset($_POST['update_player'])){

   $player_name = $_POST['player_name'];
   $player_nr = $_POST['player_nr'];
   $player_position = $_POST['player_position'];
   $player_image = $_FILES['player_image']['name'];
   $player_image_tmp_name = $_FILES['player_image']['tmp_name'];
   $player_image_folder = 'player_uploaded/'.$player_image;

   if(empty($player_name) || empty($player_nr) || empty($player_position) ){
      $message[] = 'Completați toate câmpurile';    
   }else{

      if(!empty($player_image))
      {
         $update_data = "UPDATE players SET name='$player_name', number='$player_nr', position='$player_position', image='$player_image'  WHERE id = '$id'";
         $upload = mysqli_query($conn, $update_data);
         
      }
      else
      {
         $update_data = "UPDATE players SET name='$player_name', number='$player_nr', position='$player_position'  WHERE id = '$id'";
         $upload = mysqli_query($conn, $update_data);
      }
      
      if($upload)
      {
            move_uploaded_file($player_image_tmp_name, $player_image_folder);
            header('location:crud-player.php');
      }else
      {
            $$message[] = 'Completați toate câmpurile'; 
      }
         
      
      
   }
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Modifică detalii jucători</title>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCrud.css">
   <link rel="stylesheet" href="meniu.css">
</head>
<body>
<div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m2">
                    <a class="ancora" href="lot.php">LOT</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="meciuri.php">MECIURI</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>
                
                 
            
        </div>
<?php
   if(isset($message)){
      foreach($message as $message){
         echo '<span class="message">'.$message.'</span>';
      }
   }
?>

<div class="container">


<div class="admin-product-form-container centered">

   <?php
      
      $select = mysqli_query($conn, "SELECT * FROM players WHERE id = '$id'");
      while($row = mysqli_fetch_assoc($select)){

   ?>
   
   <form action="" method="post" enctype="multipart/form-data">
      <h3 class="title">editați informațiile jucătorului</h3>
      <input type="text" class="box" name="player_name" value="<?php echo $row['name']; ?>" placeholder="Introduceți numele">
      <input type="number" min="0" class="box" name="player_nr" value="<?php echo $row['number']; ?>" placeholder="Introdduceți numărul">
      <input type="text" class="box" name="player_position" value="<?php echo $row['position']; ?>" placeholder="Introduceți poziția">
     <img src="<?php echo "player_uploaded/".$row['image']; ?>" width="100px;" >
      <input type="file" class="box" name="player_image" accept="image/png, image/jpeg, image/jpg">
     
      <input type="submit" value="UPDATEAZĂ JUCĂTORUL" name="update_player" class="btn">
      <a href="crud-player.php" class="btn">ÎNAPOI</a>
   </form>
   


   <?php }; ?>

   

</div>

</div>

</body>
</html>