<?php

@include 'config.php';

if(isset($_POST['update_update_btn'])){
   $update_value = $_POST['update_quantity'];
   $update_id = $_POST['update_quantity_id'];
   $update_quantity_query = mysqli_query($conn, "UPDATE `cart` SET quantity = '$update_value' WHERE id = '$update_id'");
   if($update_quantity_query){
      header('location:cos.php');
   };
};

if(isset($_GET['remove'])){
   $remove_id = $_GET['remove'];
   mysqli_query($conn, "DELETE FROM `cart` WHERE id = '$remove_id'");
   header('location:cos.php');
};

if(isset($_GET['delete_all'])){
   mysqli_query($conn, "DELETE FROM `cart`");
   header('location:cos.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Coșul de produse</title>
   <link rel="stylesheet" href="styleCos.css">
   <link rel="stylesheet" href="meniu.css">
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

</head>
<body>
<div class="menu">
        <div class="left-menu">
            <img class="logo-partizan-init" src="poze/logo-partizan.png">
            <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
        </div>
        <div class="mijloc-menu">
            <div class="m1">
                <a class="ancora" href="index.html">ACASĂ</a>
            </div>
            <div class="m2">
                <a class="ancora" href="lot.php">LOT</a>
            </div>
            <div class="m3">
                <a class="ancora" href="meciuri.php">MECIURI</a>
            </div>
            <div class="m4">
                <a class="ancora" href="adauga.html">ADAUGĂ</a>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a> 
            <div class="m4">
               <?php
                  $select_rows = mysqli_query($conn, "SELECT * FROM `cart`") or die('query failed');
                  $row_count = mysqli_num_rows($select_rows);
               ?>
            <a href="cos.php">
                <button class="cos"><i class="fas fa-shopping-cart"></i> <?php echo $row_count; ?></button>
            </a>
            </div>
        </div>
        
    </div>
<div class="container">

<section class="shopping-cart">

   <h1 class="heading">Lista de produse </h1>

   <table>

      <thead>
         <th>Imagine</th>
         <th>Produs</th>
         <th>Preț</th>
         <th>Cantitate</th>
         <th>Prețul total</th>
         <th>Opțiuni</th>
      </thead>

      <tbody>

         <?php 
         
         $select_cart = mysqli_query($conn, "SELECT * FROM `cart`");
         $grand_total = 0;
         if(mysqli_num_rows($select_cart) > 0){
            while($fetch_cart = mysqli_fetch_assoc($select_cart)){
         ?>

         <tr>
            <td><img src="product_uploaded/<?php echo $fetch_cart['image']; ?>" height="100" alt=""></td>
            <td><?php echo $fetch_cart['name']; ?></td>
            <td><?php echo number_format($fetch_cart['price']); ?> lei /buc</td>
            <td>
               <form action="" method="post">
                  <input type="hidden" name="update_quantity_id"  value="<?php echo $fetch_cart['id']; ?>" >
                  <input type="number" name="update_quantity" min="1"  value="<?php echo $fetch_cart['quantity']; ?>" >
                  <input type="submit"  value="UPDATE"  name="update_update_btn">
               </form>   
            </td>
            <td><?php echo $sub_total = number_format($fetch_cart['price'] * $fetch_cart['quantity']); ?> lei</td>
            <td><a href="cos.php?remove=<?php echo $fetch_cart['id']; ?>" onclick="return confirm('remove item from cart?')" class="delete-btn">ȘTERGE</a></td>
         </tr>
         <?php
           $grand_total += $sub_total;  
            };
         };
         ?>
         <tr class="table-bottom">
            <td><a href="produse.php" class="option-btn" >CONTINUAȚI CUMPĂRĂTURILE</a></td>
            <td colspan="3">TOTAL</td>
            <td><?php echo $grand_total; ?> lei</td>
            <td><a href="cos.php?delete_all" onclick="return confirm('Sunteti sigur ca doriți să ștergeți toate articolele?');" class="delete-btn">ȘTERGEȚI TOT</a></a></td>
         </tr>

      </tbody>

   </table>

   <div class="checkout-btn">
      <a href="comanda.php" style="width: 100%;" class="btn <?= ($grand_total > 1)?'':'disabled'; ?>">CONTINUAȚI CĂTRE COMANDĂ</a>
   </div>

</section>

</div>


</body>
</html>