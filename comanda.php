<?php

@include 'config.php';

if(isset($_POST['order_btn'])){

   $name = $_POST['name'];
   $number = $_POST['number'];
   $method = $_POST['method'];
   $address = $_POST['address'];
   $city = $_POST['city'];
   $country = $_POST['country'];
   $pin_code = $_POST['pin_code'];

   $cart_query = mysqli_query($conn, "SELECT * FROM `cart`");
   $price_total = 0;
   if(mysqli_num_rows($cart_query) > 0){
      while($product_item = mysqli_fetch_assoc($cart_query)){
         $product_price = number_format($product_item['price'] * $product_item['quantity']);
         $price_total += $product_price;
      };
   };
   $detail_query = mysqli_query($conn, "INSERT INTO `comanda`(name, number,  method, address, city, country, pin_code, total_price) 
   VALUES('$name','$number','$method','$address','$city','$country','$pin_code','$price_total')") or die('query failed');
     
   if(isset($_GET['delete_all'])){
      mysqli_query($conn, "DELETE FROM `cart`");
   }

   if($cart_query && $detail_query){
      echo "
      <div class='order-message-container'>
      <div class='message-container'>
         <h3>Vă mulțumim că ați comandat de la noi! Rezumatul comenzii:</h3>
         <div class='order-detail'>
            <span class='total'> Total de plată : ".$price_total." de lei  </span>
         </div>
         <div class='customer-details'>
            <p> Numele și prenumele : <span>".$name."</span> </p>
            <p> Număr de telefon : <span>".$number."</span> </p>
            <p> Adresa : <span>".$address.", ".$city.", ".$country." - ".$pin_code."</span> </p>
            <p> Metodă de plată : <span>".$method."</span> </p>
         </div>
            <a href='cos.php?delete_all' class='btn2'>Continuați cumpărăturile!</a>
         </div>
      </div>
      ";
         
   }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>checkout</title>
   <link rel="stylesheet" href="styleComada.css">

</head>
<body>
<div class="container">

<section class="checkout-form">

   <h1 class="heading">Finalizați comanda</h1>
   <?php
         $select_cart = mysqli_query($conn, "SELECT * FROM `cart`");
         $total = 0;
         $grand_total = 0;
         if(mysqli_num_rows($select_cart) > 0){
            while($fetch_cart = mysqli_fetch_assoc($select_cart)){
            $total_price = number_format($fetch_cart['price'] * $fetch_cart['quantity']);
            $grand_total = $total += $total_price;
      ?>

      <?php
         } 
      }
      ?>
      <h1 class="heading"> Total de plată: <?= $grand_total; ?> lei </h1>

   <form action="" method="post">
      <div class="flex">
         <div class="inputBox">
            <p>Numele și prenumele</p>
            <input type="text" placeholder="Introduceți numele și prenumele" name="name" required>
         </div>
         <div class="inputBox">
            <p>Telefon</p>
            <input type="text" placeholder="Introduceți nr tel" name="number" required>
         </div>
         <div class="inputBox">
            <p>Metodă de plată</p>
            <select name="method">
               <option value="Plată la livrare" selected>Plată la livrare</option>
            </select>
         </div>
         <div class="inputBox">
            <p>Adresă</p>
            <input type="text" placeholder="Introduceți adresa" name="address" required>
         </div>
         <div class="inputBox">     
            <p>Oraș  </p>
            <input type="text" placeholder="Introduceți orașul" name="city" required>
         </div>
         <div class="inputBox">
            <p>Țara</p>
            <input type="text" placeholder="Introduceți țara" name="country" required>
         </div>
         <div class="inputBox">
            <p>Cod poștal</p>
            <input type="text" placeholder="codul trebuie să conțină 6 cifre" name="pin_code" required>
         </div>
      </div>
         <input type="submit" value="Comandă acum!" name="order_btn" class="btn1">      
   </form>

</section>

</div>

   
</body>
</html>