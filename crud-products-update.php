<?php

@include 'config.php';

$id = $_GET['edit'];

if(isset($_POST['update_product'])){

    $product_name = $_POST['product_name'];
    $product_price = $_POST['product_price'];
    $product_image = $_FILES['product_image']['name'];
    $product_image_tmp_name = $_FILES['product_image']['tmp_name'];
    $product_image_folder = 'product_uploaded/'.$product_image;

    if(empty($product_name) || empty($product_price) )
    {
     $message[] = 'Completați toate câmpurile';  
   }else{

      if(!empty($product_image))
      {
         $update_data = "UPDATE products SET name='$product_name', price='$product_price', image='$product_image'  WHERE id = '$id'";
         $upload = mysqli_query($conn, $update_data);
         
      }
      else
      {
         $update_data = "UPDATE products SET name='$product_name', price='$product_price'  WHERE id = '$id'";
         $upload = mysqli_query($conn, $update_data);
      }
      
      if($upload)
      {
            move_uploaded_file($product_image_tmp_name, $product_image_folder);
            header('location:crud-products.php');
      }else
      {
         $$message[] = 'Completați toate câmpurile'; 
      }
   }
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
   <title>Modifică un produs</title>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="styleCrud.css">
   <link rel="stylesheet" href="meniu.css">

</head>
<body>
<div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m2">
                    <a class="ancora" href="lot.php">LOT</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="meciuri.php">MECIURI</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>
</div>

<?php
   if(isset($message)){
      foreach($message as $message){
         echo '<span class="message">'.$message.'</span>';
      }
   }
?>

<div class="container">


<div class="admin-product-form-container centered">

   <?php
      
      $select = mysqli_query($conn, "SELECT * FROM products WHERE id = '$id'");
      while($row = mysqli_fetch_assoc($select)){

   ?>
   
   <form action="" method="post" enctype="multipart/form-data">
      <h3 class="title">Editați informațiile despre produsul selectat</h3>
      <input type="text" class="box" name="product_name" value="<?php echo $row['name']; ?>" placeholder="Introduceți numele produsului">
      <input type="text" class="box" name="product_price" value="<?php echo $row['price']; ?>" placeholder="Introdduceți numărul">
     <img src="<?php echo "product_uploaded/".$row['image']; ?>" width="100px;" >
      <input type="file" class="box" name="product_image" accept="image/png, image/jpeg, image/jpg, image/webp">
     
      <input type="submit" value="UPDATEAZĂ PRODUSUL" name="update_product" class="btn">
      <a href="crud-products.php" class="btn">ÎNAPOI</a>
   </form>
   


   <?php }; ?>

   

</div>

</div>

</body>
</html>