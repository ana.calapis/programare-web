<?php

@include 'config.php';

if(isset($_POST['add_product'])){

   $product_name = $_POST['product_name'];
   $product_price = $_POST['product_price'];
   $product_image = $_FILES['product_image']['name'];
   $product_image_tmp_name = $_FILES['product_image']['tmp_name'];
   $product_image_folder = 'product_uploaded/'.$product_image;

   if(empty($product_name) || empty($product_price)  || empty($product_image))
   {
    $message[] = 'Completați toate câmpurile';
    }
    else{
        $insert = "INSERT INTO products (name, price, image) VALUES('$product_name', '$product_price', '$product_image')";
        $upload = mysqli_query($conn,$insert);
        if($upload){
           move_uploaded_file($product_image_tmp_name, $product_image_folder);
           $message[] = 'Produsul a fost adăugat cu succes!';
        }else{
           $message[] = 'Nu s-a putut adăuga produsul.';
        }
     }

};

if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM products WHERE id = $id");
    header('location:crud-products.php');
};


?>


<!DOCTYPE html>
<html>
    <head>
        <title>Adaugă produse</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styleCrud.css">
        <link rel="stylesheet" href="meniu.css">
        
    </head>
<body class="tot">
    <div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m2">
                    <a class="ancora" href="lot.php">LOT</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="meciuri.php">MECIURI</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>
    </div>
    <?php
         if(isset($message))
        {
            foreach($message as $message)
            {
                    echo '<span class="message">'.$message.'</span>';
            }
        }

?>
    <div class="container">

        <div class="admin-product-form-container">

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                <h3>Adaugă un nou produs</h3>
                <input type="text" placeholder="Introdu numele produsului" name="product_name" class="box">
                <input type="text" placeholder="Introdu pretul" name="product_price" class="box">
                <input type="file" accept="image/png, image/jpeg, image/jpg, image/webp" name="product_image" class="box">
                <input type="submit" class="btn" name="add_product" value="ADAUGĂ PRODUS">
                
            </form>
            
        </div>

    <?php

        $select = mysqli_query($conn, "SELECT * FROM products");
   
    ?>
   <div class="product-display">
      <table class="product-display-table">
         <thead>
         <tr>
            <th>Produs  </th>
            <th>Numele produsului</th>
            <th>Prețul</th>
            <th>Opțiuni</th>
         </tr>
         </thead>
         <?php while($row = mysqli_fetch_assoc($select)){ ?>
         <tr>
           
            <td><img src="product_uploaded/<?php echo $row['image']; ?>" height="100" alt=""></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['price']; ?></td>
            <td>
               <a href="crud-products-update.php?edit=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-edit"></i> EDITAȚI </a>
               <a href="crud-products.php?delete=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-trash-o"></i> ȘTERGEȚI</a>
            </td>
         </tr>
         <?php } ?>
      </table>
   </div>

   <a href="index.html" class="btn">ACASĂ</a>

    </div>
    </body>
</html>