<?php

@include 'config.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Meciuri</title>
    <link rel ="stylesheet" href ="meniu.css"> 
    <link rel ="stylesheet" href ="styleMeci.css"> 
    <link href ="poze/logo-partizan.png">
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Karla:wght@400;500;700&display=swap" rel="stylesheet">
</head>
<body class="body">
<div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m2">
                    <a class="ancora" href="lot.php">LOT</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>
                
                 
            
        </div>
    <div class="aspect-meciuri"> 
        <div class="titlu"><h1>Meciurile din EuroLeague BC Partizan 2023-2024</h1></div>
    </div>
    <div class="meciuri" style="padding-bottom: 90px;">
    <?php
        $select = mysqli_query($conn, "SELECT * FROM matches");
    ?>
    <?php while($row = mysqli_fetch_assoc($select)){ ?>
        <div class="meci">
            <div class="data">
                <p class="text-data"><?php echo $row['date']; ?></p>
            </div>
            <div class="sala">
                <p class="text-sala"><?php echo $row['hall']; ?></p>
            </div>
            <div class="logo1">
                <img class="poza-logo1" src="game_uploaded/<?php echo $row['logo1']; ?>" height="70" alt="">
            </div>
            <div class="team1">
                <p class="text-echipa1"><?php echo $row['team1']; ?></p>   
            </div>
            <div class="score1">
                <p class="text-scor1"><?php echo $row['score1']; ?></p>
            </div>
            <div class="vs">
                <p>-</p>
            </div>
            <div class="score2">
                <p class="text-scor2"><?php echo $row['score2']; ?></p>
            </div>
            <div class="team2">
                <p class="text-echipa2"><?php echo $row['team2']; ?></p>   
            </div>
            <div class="logo2">
                <img class="poza-logo2" src="game_uploaded/<?php echo $row['logo2']; ?>" height="70" alt="">
            </div>
        </div> 
    <?php } ?>
    </div>
</body>
</html>
    