<?php

@include 'config.php';

if(isset($_POST['add_games'])){

   $game_hall = $_POST['game_hall'];
   $game_date = $_POST['game_date'];

   $game_logo1 = $_FILES['game_logo1']['name'];
   $game_logo1_tmp_name = $_FILES['game_logo1']['tmp_name'];
   $game_logo1_folder = 'game_uploaded/'.$game_logo1;
    
   $game_team1 = $_POST['game_team1'];
   $game_score1 = $_POST['game_score1'];
   $game_score2 = $_POST['game_score2'];
   $game_team2 = $_POST['game_team2'];

   $game_logo2 = $_FILES['game_logo2']['name'];
   $game_logo2_tmp_name = $_FILES['game_logo2']['tmp_name'];
   $game_logo2_folder = 'game_uploaded/'.$game_logo2;


   if(empty($game_hall) || empty( $game_date)  || empty($game_logo1)  || empty($game_team1) 
    || empty($game_score1)  || empty($game_score2)  || empty($game_team2)  || empty($game_logo2))
   {
    $message[] = 'Completați toate câmpurile';
    }
    else{
        $insert = "INSERT INTO matches (hall, date, logo1, team1, score1, score2, team2, logo2) 
        VALUES('$game_hall', '$game_date', '$game_logo1','$game_team1', '$game_score1', '$game_score2', '$game_team2', '$game_logo2')";
        $upload = mysqli_query($conn,$insert);
        if($upload){
           move_uploaded_file($game_logo1_tmp_name, $game_logo1_folder);
           move_uploaded_file($game_logo2_tmp_name, $game_logo2_folder);
           $message[] = 'Meciul a fost adăugat cu succes!';
        }else{
           $message[] = 'Nu s-a putut adăuga meciul.';
        }
     }

};

if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    mysqli_query($conn, "DELETE FROM matches WHERE id = $id");
    header('location:crud-games.php');
};


?>


<!DOCTYPE html>
<html>
    <head>
        <title>Adaugă meciuri</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styleCrud.css">
        <link rel="stylesheet" href="meniu.css">
    </head>
<body class="tot">
    <div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m2">
                    <a class="ancora" href="lot.php">LOT</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="meciuri.php">MECIURI</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>
    </div>
    <?php
         if(isset($message))
        {
            foreach($message as $message)
            {
                    echo '<span class="message">'.$message.'</span>';
            }
        }
    ?>
    <div class="container">

        <div class="admin-product-form-container">

            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                <h3>Adaugă un nou meci</h3>
                <input type="text" placeholder="Introdu sala" name="game_hall" class="box">
                <input type="date" placeholder="Introdu data și ora" name="game_date" class="box">
                <input type="file" accept="image/png, image/jpeg, image/jpg" name="game_logo1" class="box">
                <input type="text" placeholder="Nume echipa A" name="game_team1" class="box">
                <input type="text" placeholder="Scor echipa A" name="game_score1" class="box">
                <input type="text" placeholder="Scor echipa B" name="game_score2" class="box">
                <input type="text" placeholder="Nume echipa B" name="game_team2" class="box">
                <input type="file" accept="image/png, image/jpeg, image/jpg, image/webp, image/html, image/svg" name="game_logo2" class="box">
                <input type="submit" class="btn" name="add_games" value="ADAUGĂ MECI">
                
            </form>
            
        </div>

    <?php

        $select = mysqli_query($conn, "SELECT * FROM matches");
   
    ?>
   <div class="product-display">
      <table class="product-display-table">
         <thead>
         <tr>
              <th>Meciuri</th>
         </tr>
         </thead>
         <?php while($row = mysqli_fetch_assoc($select)){ ?>
         <tr>
            <td><?php echo $row['hall']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><img src="game_uploaded/<?php echo $row['logo1']; ?>" height="50" alt=""></td>
            <td><?php echo $row['team1']; ?></td>
            <td><?php echo $row['score1']; ?></td>
            <td><?php echo $row['score2']; ?></td>
            <td><?php echo $row['team2']; ?></td>
            <td><img src="game_uploaded/<?php echo $row['logo2']; ?>" height="50" alt=""></td>
            <td>
               <a href="crud-games-update.php?edit=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-edit"></i> EDITAȚI </a>
               <a href="crud-games.php?delete=<?php echo $row['id']; ?>" class="btn"> <i class="fa fa-trash-o"></i> ȘTERGEȚI </a>
            </td>
         </tr>
         <?php } ?>
      </table>
   </div>

   <a href="index.html" class="btn">ACASĂ</a>

    </div>
    </body>
</html>