<?php

@include 'config.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Lotul de jucători</title>
        <link rel ="stylesheet" href ="meniu.css">
        <link rel ="stylesheet" href ="styleLotNou.css">
        <link href ="poze/logo-partizan.png">
        <meta charset="UTF-8">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Karla:wght@400;500;700&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="menu">
            <div class="left-menu">
                <img class="logo-partizan-init" src="poze/logo-partizan.png">
                <a href="https://www.euroleaguebasketball.net/euroleague/" target="_blank"> <img class="logo-euroleague" src="poze/logo-euroleague.png"></a>
            </div>
            <div class="mijloc-menu">
                <div class="m1">
                    <a class="ancora" href="index.html">ACASĂ</a>
                </div>
                <div class="m3">
                    <a class="ancora" href="meciuri.php">MECIURI</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="produse.php">PRODUSE</a>
                </div>
                <div class="m4">
                    <a class="ancora" href="adauga.html">ADAUGĂ</a>
                </div>
            </div>
            <a href="logout.php">
                <button class="button">LOG OUT</button>
            </a>          
        </div>
  
    <div class="poza-cu-echipa">
        <img class="imagine-cu-echipa" src="poze/players-2022-2023.jpg">
    </div>      

    <div class="lotul-de-jucatori">
        <?php @include 'config.php'; 
                    $select = mysqli_query($conn, "SELECT * FROM players");
                    while($row = mysqli_fetch_assoc($select)){ ?>
                <div class="jucator">
                    <div class="poza-jucator">
                        <img class="imagine" src="player_uploaded/<?php echo $row['image']; ?>">
                    </div>
                    <div class="date">
                        <p class="nume"><?php echo $row['name']; ?></p>
                        <p class="nr"><?php echo $row['number']; ?></p>
                        <p class="pozitie"><?php echo $row['position']; ?></p>
                    </div>
                
                </div>
                <?php } ?>

    </div>

        
       
        
    </body>
</html>